var platypusCreek = (function(){
	var s,
		platypusCreek = {
			settings: {
				imageSvg 		: '.image-svg',
				imageBg 		: '.image-background',
				//headerPrimary 	: '.header-primary',
				inputSelect 	: 'select',
				datePicker 		: '.input-datepicker',
				fileUpload 		: '.contain-file-upload',
				map 			: '.map',
				ajaxModal 		: '[data-toggle=modal-ajax]',
            	inputMask 		: '.input-mask',
				modal 			: '.modal',
				validate 		: '.form-validate',
				tile 			: '.contain-isotope',
				popNotification : '.pop-notification'
			},
			init: function(){
				s = this.settings,
				this.imageSvg(),
				this.imageBg();
				this.headerPrimary();
				this.inputSelect();
				this.datePicker();
				this.fileUpload();
				this.map();
				this.ajaxModal();
				this.maskInput();
				this.modalCallback();
				this.form();
				this.validation();
				this.tile();
				this.popNotification();
			},
			imageSvg: function(){
				$(s.imageSvg).each(function(){
					if( typeof SVGInjector === 'function' ) SVGInjector(this);
				});
			},
			imageBg: function(){
				$(s.imageBg).each(function(){
					var src = $(this).attr('src');
					$(this)
						.hide()
						.attr('src','images/blank.png')
						.css('background-image', 'url(' + src + ')')
						.addClass('image-background-active')
						.show();
				});
			},
			headerPrimary: function(){
				var headerHeight = $(s.headerPrimary).outerHeight();
				var offset = 33;
				$(window).on('scroll', function(){
					var showHeader = $(this).scrollTop() > (headerHeight + offset);
					$(s.headerPrimary)
						.toggleClass( 'in', showHeader )
						.find('[data-toggle=dropdown]').each(function(){
							if( $(this).parent().is('.open') ) $(this).dropdown('toggle');
						})
				});
			},
			inputSelect: function() {
                $(s.inputSelect)
                    .each(function() {
                        if ( !$().chosen ) return;
                        
                        var noSearch = typeof $(this).data('no-search') !== 'undefined' ? $(this).data('no-search') : "false";

                        $(this)
                            .each(function() {
                                var e = $(this)
                                    .attr('data-nosearch', noSearch),
                                    t = e.attr("data-nosearch") === "true" ? !0 : !1,
                                    n = {};

                                n.width = '100%';
                                t && (n.disable_search_threshold = 9999999);
                                var theChosen = e.chosen(n);
                            });
                    });
            },
			datePicker: function() {
                $(s.datePicker)
                    .each(function() {
                        if ( !$().datepicker ) return;

                        $(this).datepicker({
                        	startDate: 'today',
                        	autoclose: true
                        });
                    });
            },
			fileUpload: function() {
                $(s.fileUpload)
                    .each(function() {
                        if ( !$().dropzone || $(this).is('.dropzone-init') ) return;

                       var el = {
							_field 				: $(this),
							_parent 			: $(this).parents('.contain-upload-' + $(this).data('type')),
							_action 			: $(this).data('url'),
							_thumbnailWidth 	: $(this).data('thumbnail-width'),
							_thumbnailHeight 	: $(this).data('thumbnail-height'),
							_maxFiles 			: $(this).data('max-files'),
							_paramName			: $(this).data('param-name'),
							_progressLabel      : $('<span/>').addClass('progress-label')
						};

						el._progressLabel.appendTo( el._parent );

						var localDropzone = new Dropzone(el._field[0], {
							url 				: el._action,
							thumbnailWidth 		: el._thumbnailWidth || null,
							thumbnailHeight 	: el._thumbnailHeight || null,
							maxFiles 			: el._maxFiles || 1,
							uploadMultiple 		: el._maxFiles > 1,
							maxThumbnailFilesize: 50,
							acceptedFiles 		: 'image/*',
							clickable 			: el._field.data('clickable'),
							previewsContainer 	: el._field.data('preview'),
							addRemoveLinks		: true,
							paramName 			: el._paramName
							//previewTemplate: '<div class="background-seamless upload-progress-item">' + '<div class="progress-bar background-primary" data-dz-uploadprogress ></div>' + '</div>' + '<img class="upload-thumbnail" data-dz-thumbnail />' + '<div class="upload-preview" data-dz-name></div>' + '<div class="upload-error" data-dz-errormessage></div>' + '<span data-dz-remove>&times;</span>'
						});

						localDropzone
							.on('processing', function(){
								if( el._parent.length > 0 ) el._parent.addClass('is-processing')
							})
							.on('thumbnail', function(){
								if( el._parent.length > 0 ) el._parent.addClass('has-thumbnail').removeClass('is-dragging')
							})
							.on('reset', function(){
								if( el._parent.length > 0 ) el._parent.removeClass('has-thumbnail max is-dragging')
							})
							.on('dragover', function(){
								if( el._parent.length > 0 ) el._parent.addClass('is-dragging')
							})
							.on('dragleave', function(){
								if( el._parent.length > 0 ) el._parent.removeClass('is-dragging')
							})
							.on('complete', function(file){
								if( el._parent.length > 0 ){
									el._parent.removeClass('is-processing');
									el._parent.find('.progress-label').empty();
								}
							})
							.on('removedfile', function(file){
								if( el._parent.length > 0 ) el._parent.removeClass('max')
							})
							.on('maxfilesreached', function(file){
								if( el._parent.length > 0 ) el._parent.addClass('max')
							})
							.on('maxfilesexceeded', function(file){
								if( el._parent.length > 0 ) el._parent.addClass('max')
								$(file.previewElement).remove();
							})
							.on('uploadprogress', function(file, progress){
								var currentProgress = Math.round( progress );
								el._parent.find('.progress-label').html( Math.round( progress ) === 100 ? 'please wait...' : currentProgress + '% completed...');
							})
							.on('success', function(file, response){
								for( var i in response.files ){
									$('[name="input_' + i + '"]').val(response.files[i]);
								}
							});

						el._field.addClass('dropzone-init');
                    });
            },
            map: function(){
            	$(s.map).each(function(){
					var geo = { lat: parseFloat( $(this).data('lat') ), lng: parseFloat( $(this).data('lng') ) };
					var map = new google.maps.Map($(this)[0], {
					  center: geo,
					  zoom: $(this).data('zoom') || 17,
					  scrollwheel: false
					});

					// Create a marker and set its position.
					  var marker = new google.maps.Marker({
					    map: map,
					    position: geo
					  });
				});
            },
            ajaxModal: function(){
				$('[data-toggle="modal"]').each(function() {
					var link 		= $(this),
						url 		= $(this).attr('href') || $(this).data('target'),
						modalName 	= url.substr(url.lastIndexOf('/') + 1),
						modalAfter;

            		if (url.indexOf('#') == 0) return;

            		modalName = modalName.replace('.html', '');

            		modalAfter = function(){
            			platypusCreek.imageSvg(),
						platypusCreek.inputSelect();
						platypusCreek.datePicker();
						platypusCreek.fileUpload();
						platypusCreek.ajaxModal();
						platypusCreek.maskInput();
						platypusCreek.validation();
						platypusCreek.popNotification();
            		};

            		$.get(url, function(data) {
						var modalDom 	= $(data).find('.modal-html').html();
						var modalClose 	= '<span data-dismiss="modal"><img src="images/svg/times.svg" class="image-svg fill-clean width-52"></span>';

						link.attr('href', '#' + modalName);
						link.attr('data-target', '#' + modalName);

						if(link.is('form')) link.removeAttr('data-toggle');
						if( $('#' + modalName).html() === '' ){
							$(modalDom).appendTo($('#' + modalName)).each(function(){
								$(this).find('.modal-content').prepend( $(modalClose) );
								modalAfter();
							});
						}
					});
				});
            },
            maskInput: function () {
	            $(s.inputMask).each(function () {
	                if (!$().mask) return;

	                var input = $(this),
	                    format = input.data('mask-format');

	                input.mask(format, {autoclear: false});
	            });
	        },
            modalCallback: function(){
            	$(s.modal)
					.on('show.bs.modal', function(evt){
						if( evt.target.nodeName === "INPUT" ) return;
						$('.modal:visible').modal('hide');

						if( $(this).is('#modal-service-collision-contact-details') ){
							var serviceType = $(evt.relatedTarget).data('service-type');
							if( serviceType ) $(evt.currentTarget).find('[name="service_type"]').val(serviceType);
						}
					})
					.on('hidden.bs.modal', function(evt){
						window.previousModal = evt.currentTarget;
						$('.btn-modal-back').attr('href', '#' + window.previousModal.id);
					});

				$(document).on('click', '.btn-respray-plan', function(){
					var plan = $(this).data('plan');
					$('.chosen-respray-plan.in').removeClass('in');
					$('.chosen-respray-plan.plan-' + plan).addClass('in');
				});
            },
            form: function(){
            	$(document).on('change', '[name="no_claim_number"]', function(){
            		var noClaimNumber = $(this).is(':checked');
            		$('[name="claim_number"]').prop('disabled', noClaimNumber);
            	});

            	$(document).on('change', '[name="qld_plate"]', function(){
            		var isQld = $(this).val() === "yes";
            		$('[name="vin_number"]').prop('disabled', isQld);
            		$('#vin-details').collapse( !isQld ? 'show' : 'hide');
            	});
            },
            validation: function(){
            	$(s.validate).each(function(){
            		if (!$().validate) return;

	                var form 		= $(this),
	                    next 		= form.attr('data-target'),
	                    finish 		= form.attr('data-finish'),
	                    action 		= form.attr('action'),
	                    services 	= [
	                    	{
	                    		// service type : 0
	                    		"label"  : "Others"
	                    	},
	                    	{
	                    		// service type : 1
	                    		"label"  : "Collision Repair - Through Insurance"
	                    	},
	                    	{
	                    		// service type : 2 ("Not sure")
	                    		"label"  : "Collision Repair - Private",
	                    		"excluded_fields" : [
	                    			"who_at_fault",
	                    			"insurance_company",
	                    			"claim_number",
	                    			"no_claim_number"
	                    		]
	                    	},
	                    	{
	                    		// service type : 3
	                    		"label"  : "Resprays - Same Colour",
	                    		"excluded_fields" : [
                                    "input_file_sample[0]",
                                    "input_file_sample[1]",
                                    "input_file_sample[2]",
                                    "old_paint_code",
                                    "new_paint_code"
	                    		]
	                    	},
	                    	{
	                    		// service type : 4
	                    		"label"  : "Resprays - Change Colour"
	                    	},
	                    	{
	                    		// service type : 5
	                    		"label"  : "Touchups"
	                    	}
	                    ];


	                var validator = form.validate({
	                    ignore 		: ".ignore, :hidden:not(select)",
	                    focusCleanup: true,
	                    focusInvalid: false
	                });

	                if( next ){
	                	form.data('validator').settings.submitHandler = function (_form) {
	                		var next 	= $(_form).attr('data-target');
	                		var fields 	= $(_form).serializeObject();

	                		if( finish ){
	                			var selectedService = services[fields['service_type']];
	                			if( selectedService.excluded_fields ){
	                				for (var i in selectedService.excluded_fields){
	                					var key = selectedService.excluded_fields[i];
	                					delete fields[key];
	                				}
	                			}

	                			$(next).modal()
	                			return;
	                		}

	                		$(next).modal();

	                		for( var i in fields ){
	                			var field = $(next).find('[name=' + i + ']');
	                			if( field.length > 0 ) field.val( fields[i] );
	                		}
	                	}
	                }

	                $(document).on('click', '[data-dismiss=modal]', function(evt){
						form.find('.wrap-upload-preview').empty();
						form.find('#vin-details').collapse('show');
						form.find('[name=vin_number]').prop('disabled', false);
						form[0].reset();
					});
            	});
            },
            tile: function(){
            	$(s.tile).each(function(){
            		var _this = $(this),
            			tile  = _this.isotope({
            				itemSelector : '.isotope-item',
            				layoutMode: 'packery',
            				packery: {
            					gutter: 20
            				}
            			});

            		_this.imagesLoaded().progress( function() {
						_this.isotope('layout');
					});
            	});
            },
            popNotification: function(){
		        $(s.popNotification).each(function () {
		            var popover = $(this),
						content = popover.data('pop-content');
		            $(content).hide();
		            popover.popover({
		                html: true,
		                content: function () {
		                    return $(content).html();
		                }
		            });
		        });
            }
		};

	return platypusCreek;
})();

$(function(){
	platypusCreek.init();
});